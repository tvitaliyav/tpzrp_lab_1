import select
import socket
import numpy as np
import cv2
Image = 'ImageFromServer.jpg'
FinalImage = 'ImageFromServerFinal.jpg'
Host = '127.0.0.1'
Port = 6667
ConnectedClientsSockets = []
ServerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
ServerSocket.bind((Host, Port))
ServerSocket.listen(1)
ConnectedClientsSockets.append(ServerSocket)
while True:
    ReadSockets, WriteSockets, ErrorSockets = select.select(ConnectedClientsSockets, [], [])
    for Socket in ReadSockets:
        if Socket == ServerSocket:
            NewSocket, ClientAddress = ServerSocket.accept()
            ConnectedClientsSockets.append(NewSocket)
        else:
            try:
                Data = Socket.recv(4096)
                try:
                    String = Data.decode()
                except:
                    String = ''
                if Data:
                    if String.startswith('Size'):
                        Socket.sendall('Got Size'.encode())
                    elif String.startswith('Stop'):
                        Socket.shutdown()
                    else:
                        ImageFile = open(Image, 'wb')
                        ImageFile.write(Data)
                        Data = Socket.recv(40960000)
                        if not Data:
                            ImageFile.close()
                            break
                        ImageFile.write(Data)
                        ImageFile.close()
                        Socket.sendall('Got Image'.encode())
                        I = cv2.imread(Image, 1)  # 1/ -1: color mode; 0: gray mode
                        rez_I = np.array(255 * I, 'uint8')
                        output_image = cv2.medianBlur(rez_I.astype(np.float32), 3, 0)
                        res_out = np.array(255 * output_image, 'uint8')
                        cv2.imwrite(FinalImage, res_out)
                        Socket.shutdown()
            except:
                Socket.close()
                ConnectedClientsSockets.remove(Socket)
                continue
ServerSocket.close()
