import socket
Image = 'ImageFromClient.jpg'
Host = '127.0.0.1'
Port = 6666
Socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ServerAddress = (Host, Port)
Socket.connect(ServerAddress)
try:
    ImageFile = open(Image, 'rb')
    ByteSequence = ImageFile.read()
    Size = len(ByteSequence)
    Socket.sendall(('Size %s' % Size).encode())
    Answer = Socket.recv(40960000).decode()
    print('Answer = %s' % Answer)
    if Answer == 'Got Size':
        Socket.sendall(ByteSequence)
        Answer = Socket.recv(40960000).decode()
        print('Answer = %s' % Answer)
        if Answer == 'Got Image':
            Socket.sendall('Stop'.encode())
            print('Image successfully send to server')
    ImageFile.close()
finally:
    Socket.close()
