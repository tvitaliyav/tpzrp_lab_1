import select
import socket
import numpy as np
import cv2
from skimage.util import random_noise
Image = 'ImageFromInterceptor.jpg'
SPImage = 'ImageFromInterceptorWithNoise.jpg'
Host = '127.0.0.1'
Port = 6666
SPort = 6667
Socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ServerAddress = (Host, SPort)
Socket.connect(ServerAddress)

ConnectedClientsSockets = []
ServerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
ServerSocket.bind((Host, Port))
ServerSocket.listen(1)
ConnectedClientsSockets.append(ServerSocket)
while True:
    ReadSockets, WriteSockets, ErrorSockets = select.select(ConnectedClientsSockets, [], [])
    for Socket_ in ReadSockets:
        if Socket_ == ServerSocket:
            NewSocket, ClientAddress = ServerSocket.accept()
            ConnectedClientsSockets.append(NewSocket)
        else:
            try:
                Data = Socket_.recv(4096)
                try:
                    String = Data.decode()
                except:
                    String = ''
                if Data:
                    if String.startswith('Size'):
                        Socket_.sendall('Got Size'.encode())
                    elif String.startswith('Stop'):
                        Socket_.shutdown()
                    else:
                        ImageFile = open(Image, 'wb')
                        ImageFile.write(Data)
                        Data = Socket_.recv(40960000)
                        if not Data:
                            ImageFile.close()
                            break
                        ImageFile.write(Data)
                        ImageFile.close()
                        I = cv2.imread(Image, 1);  # 1/ -1: color mode; 0: gray mode
                        sp = random_noise(I, 's&p', None, True)
                        Socket_.sendall('Got Image'.encode())
                        rez_sp = np.array(255 * sp, 'uint8')
                        cv2.imwrite(SPImage, rez_sp)
                        try:
                            ImageFileSP = open(SPImage, 'rb')
                            ByteSequence = ImageFileSP.read()
                            Size = len(ByteSequence)
                            Socket.sendall(('Size %s' % Size).encode())
                            Answer = Socket.recv(40960000).decode()
                            print('Answer = %s' % Answer)
                            if Answer == 'Got Size':
                                Socket.sendall(ByteSequence)
                                Answer = Socket.recv(40960000).decode()
                                print('Answer = %s' % Answer)
                                if Answer == 'Got Image':
                                    Socket.sendall('Stop'.encode())
                                    print('Image successfully send to server')
                            ImageFileSP.close()
                        finally:
                            Socket.close()
                        Socket_.shutdown()
            except:
                Socket_.close()
                ConnectedClientsSockets.remove(Socket_)
                continue
ServerSocket.close()
